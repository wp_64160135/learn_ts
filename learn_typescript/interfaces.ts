interface Rectangle {
    height: number,
    width: number
}

interface ColorRectangle extends Rectangle {
    color: string
}
const rectangle: Rectangle = {
    height: 20,
    width: 10
};

console.log(rectangle);

const coloredRectangle: ColorRectangle = {
    width: 20,
    height: 10,
    color: "red"
}

console.log(coloredRectangle);