const car: { type: string, model: string, year?: number } = {
    type: "Toyota",
    model: "Corolla",
    // year: 2009
};

// car.type = "Ford";
console.log(car);